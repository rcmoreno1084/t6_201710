package model.data_structures;

public class GenericNode<K,V> {

	private GenericNode<K,V> siguiente;
	private GenericNode<K,V> anterior;

	private V item;
	private K key;

	public GenericNode(){
		siguiente = null;
		anterior = null;
		key = null;
		item = null;
	}

	public GenericNode<K,V> darSiguiente(){
		return siguiente;
	}

	public GenericNode<K,V> darAnterior(){
		return anterior;
	}

	public void setSiguiente (GenericNode<K,V> pSig){
		siguiente = pSig;
	}

	public void setAnterior (GenericNode<K,V> pAnt){
		anterior = pAnt;
	}

	public void setItem(V pItem){
		item = pItem;
	}

	public V darItem(){
		return item;
	}
	
	public void setKey(K pKey){
		key=pKey;
	}
	
	public K darKey(){
		return key;
	}
}
