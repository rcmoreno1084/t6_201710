package model.data_structures;

import java.util.Iterator;

public class ListaLlaveValorSecuencial<K extends Comparable<K>, V> implements Iterable<K>{
	private GenericNode<K,V> primero;
	private GenericNode<K,V> ultimo;
	private int size = 0;

	@Override
	public Iterator<K> iterator() {

		Iterator<K> it = new Iterator<K>(){

			GenericNode<K,V> currentNode= primero;
			int index = -1;
			@Override
			public boolean hasNext() {
				if(primero == null || currentNode.darSiguiente()==null) return false;
				else return true;
			}

			@Override
			public K next() {
				if (index == -1){
					currentNode = primero;
					index++;
					return (K) primero.darKey();
				}else if (this.hasNext()){
					currentNode = currentNode.darSiguiente();
					index++;
					return (K) currentNode.darKey();
				}else
					return null;
			}

			@Override
			public void remove() {
				if (currentNode == primero){
					primero = primero.darSiguiente();
					primero.setAnterior(null);
				}
				else{
					currentNode.darAnterior().setSiguiente(currentNode.darSiguiente());
					currentNode.darSiguiente().setAnterior(currentNode.darAnterior());
					currentNode = currentNode.darAnterior();

				}

			}

		};

		return it;
	}
	
	public int darTamanio(){
		return size;
	}

	public boolean estaVacia(){
		return primero==null;
	}
	
	public boolean tieneLlave(K llave){
		Iterator<K> iter = this.iterator();
		boolean res = false;
		while (iter.hasNext()&&!res){
			K llaveIt = iter.next();
			if(llaveIt.compareTo(llave)==0){
				res = true;
			}
		}
		return res;
	}
	
	public V darValor(K pLlave){
		if (estaVacia()||!tieneLlave(pLlave)) return null;
		GenericNode<K,V> currentNode = primero;
		V res = null;
		while(currentNode!=null&& res !=null){
			if(currentNode.darKey().compareTo(pLlave)==0) res = currentNode.darItem();
			currentNode.darSiguiente();
		}
		return res;
	}
	
	public void insertar(K llave, V valor){
		if(tieneLlave(llave)){
			GenericNode<K,V> currentNode = primero;
			boolean agregado = false;
			while(currentNode!=null&& !agregado){
				if(currentNode.darKey().compareTo(llave)==0){
					currentNode.setItem(valor);
					agregado = true;
				}
				currentNode.darSiguiente();
			}
		}if(tieneLlave(llave)&& valor == null){
			Iterator<K> iter = this.iterator();
			boolean removido = false;
			while(iter.hasNext()&&!removido){
				K llaveIt = iter.next();
				if(llaveIt.compareTo(llave)==0){
					iter.remove();
					size--;
					removido =true;
				}
			}
		}else{
			GenericNode<K,V> add = new GenericNode<K,V>();
			add.setItem(valor);
			add.setKey(llave);
			ultimo.setSiguiente(add);
			size++;
		}
	}
}
